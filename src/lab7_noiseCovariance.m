clear
clc
close all

%% Pt1: estimating the sample mean and variance of the gyrocope readings
load('imuData_still');
% Unpack data
time = imuData.time;
gyroVelocity = imuData.velocity;
voltage = imuData.voltage;

N = length(time);
x = [gyroVelocity];

% Compute sample mean
mean_s_gyro = mean(x);
% Compute sample variance. Use Bessel's correction
var_s_gyro = var(x);

%% Pt2: estimating variance of accelerometer angle measurements
load('imuData_moving1');

% Unpack data
time = imuData.time;
accAngle = imuData.angle;
gyroVelocity = imuData.velocity;
voltage = imuData.voltage;
potAngle = (voltage-2.125)*(240/3.3)*(pi/180);
N = length(time);

% Correct angle offset
pot_angle_init = mean(potAngle(1:200));
acc_angle_init = mean(accAngle(1:200));
potAngle = potAngle + (acc_angle_init - pot_angle_init);

figure()
plot(time, accAngle, time, potAngle)
legend('AccAngle', 'PotAngle')

% Compute the angle measurement 'noise'
angle_noise = accAngle - potAngle;

figure()
plot(time, accAngle, time, potAngle, time, angle_noise)
legend('AccAngle', 'PotAngle', 'Noise')

x = [angle_noise];
% Compute sample mean
mean_s_angle = mean(x);
% Compute sample variance. Use Bessel's correction
var_s_angle = var(x);

%% measurement noise covariance
R = [var_s_angle 0; 0 var_s_gyro];
