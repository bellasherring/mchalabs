clear; clc; close all

% load and unpack data
load('imuData_moving1');

time = imuData.time;
accAngle = imuData.angle;
gyroVelocity = imuData.velocity;
voltage = imuData.voltage;
potAngle = (voltage-2.125)*(240/3.3)*(pi/180);
N = length(time);

% use first 200 samples to correct offset
pot_angle_init = mean(potAngle(1:200));
acc_angle_init = mean(accAngle(1:200));
potAngle = potAngle + (acc_angle_init - pot_angle_init);

% set up optimisation
% define cost
get_angle = @(x) x(:,2);
get_gyro = @(x) x(:,1) + x(:,3);
cost = @(params) norm(potAngle - get_angle(runKF(accAngle, gyroVelocity, N, params))) + norm(gyroVelocity - get_gyro(runKF(accAngle, gyroVelocity, N, params)));
% Run optimisation
params0 = [1e-4; 0; 1e-4; 0; 0; 1e-4];
params_opt = fminunc(cost, params0);

L_opt = [params_opt(1) 0 0; params_opt(2) params_opt(3) 0; params_opt(4) params_opt(5) params_opt(6)];
Q_opt = L_opt*L_opt';

[x, p] = runKF(accAngle, gyroVelocity, N, params_opt);

figure()
subplot(2,1,1)
plot(time, potAngle, time, x(:,2), time, accAngle)
title('Kalman Filter on Moving1 Data After Optimisation')
grid on
legend('Potentiometer Angle', 'Kalman Filter Angle', 'Accelerometer Angle', 'Location', 'southwest')
subplot(2,1,2)
plot(time, gyroVelocity, time, x(:,1))
grid on
legend('Gyro Velocity', 'Kalman Filter Velocity', 'Location', 'best')

% load and unpack data
load('imuData_still');

time = imuData.time;
accAngle = imuData.angle;
gyroVelocity = imuData.velocity;
voltage = imuData.voltage;
potAngle = (voltage-2.125)*(240/3.3)*(pi/180);
N = length(time);

% use first 200 samples to correct offset
pot_angle_init = mean(potAngle(1:200));
acc_angle_init = mean(accAngle(1:200));
potAngle = potAngle + (acc_angle_init - pot_angle_init);

% set up optimisation
% define cost
get_angle = @(x) x(:,2);
get_gyro = @(x) x(:,1) + x(:,3);
cost = @(params) norm(potAngle - get_angle(runKF(accAngle, gyroVelocity, N, params))) + norm(gyroVelocity - get_gyro(runKF(accAngle, gyroVelocity, N, params)));
% Run optimisation
params0 = [1e-4; 0; 1e-4; 0; 0; 1e-4];
params_opt = fminunc(cost, params0);

L_opt = [params_opt(1) 0 0; params_opt(2) params_opt(3) 0; params_opt(4) params_opt(5) params_opt(6)];
Q_opt = L_opt*L_opt';

[x, p] = runKF(accAngle, gyroVelocity, N, params_opt);

figure()
subplot(2,1,1)
plot(time, potAngle, time, x(:,2), time, accAngle)
title('Kalman Filter on Still Data')
grid on
legend('Potentiometer Angle', 'Kalman Filter Angle', 'Accelerometer Angle', 'Location', 'southwest')
subplot(2,1,2)
plot(time, gyroVelocity, time, x(:,1))
grid on
legend('Gyro Velocity', 'Kalman Filter Velocity', 'Location', 'southwest')

%% Functions
function [x_KF, P_KF] = runKF(angle_accel, velocity_gyro, N, params)
    % unpack params into Cholesky decomposition of Q
    L = [params(1) 0 0; params(2) params(3) 0; params(4) params(5) params(6)];
    % compute process noise Q from L
    Q = L*L';
    
    % Define continuous time model
    Ac = [0, 0, 0; 1, 0, 0; 0, 0, 0];
    Bc = [];
    C = [0, 1, 0; 1, 0, 1];
    % Discretise model
    T = 0.005; % 1/200
    [Ad, ~] = c2d(Ac, Bc, T);
    
    % Define Kalman filter initial conditions
    % Initial state estimate
    xm = [0; 0; 0];
    % Initial estimate error covariance
    Pm = 1*eye(3);
    % Measurement noise covariance
    R = [0.0502438689662925, 0; 0, 3.45463849632733e-06];
    
    % Allocate space to save results
    x_KF = zeros(N,3);
    P_KF = zeros(3,3,N);
    I = 1*eye(3);
    
    % Run Kalman Filter
    for i=1:N
        % Pack measurement vector
        yi = [angle_accel(i); velocity_gyro(i)];
        % no input
        
        % CORRECTION
        % Compute Kalman gain
        Kk = Pm*C.'/(C*Pm*C.' + R);
        % compute corrected state estimate
        xp = xm + Kk*(yi - C*xm);
        % Compute new measurement error covariance
        Pp = (I - Kk*C)*Pm*(I - Kk*C).' + Kk*R*Kk.';
        
        % PREDICTION
        % Predict next state
        xm = Ad*xp;
        % Compute prediction error covariance
        Pm = Ad*Pp*Ad.' + Q;
        
        % Store results
        x_KF(i,:) = xp.';
        P_KF(:,:,i) = Pm;
    end
end