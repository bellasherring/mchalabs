#include <stdint.h>
#include <stdlib.h>
#include <math.h>
#include "stm32f4xx_hal.h"
#include "cmsis_os2.h"
#include "uart.h"
#include "motor.h"

// definition variables
static GPIO_InitTypeDef GPIO_PA6_Struct;
static TIM_HandleTypeDef htim3;
static TIM_OC_InitTypeDef timerConfigPWM;

static GPIO_InitTypeDef GPIO_EncoderPins_Struct;
int32_t enc_count = 0;

void motor_PWM_init(void)
{
    /* Enable TIM3 clock */
    __TIM3_CLK_ENABLE();
    /* Enable GPIOA clock */
    __HAL_RCC_GPIOA_CLK_ENABLE();

    /* Initialise PA6 with:
        x Pin 6
        x Alternate function push-pull mode
        x No pull
        x High frequency
        x Alternate function 2 - Timer 3*/
	GPIO_PA6_Struct.Pin = GPIO_PIN_6;
	GPIO_PA6_Struct.Mode = GPIO_MODE_AF_PP;
    GPIO_PA6_Struct.Pull = GPIO_NOPULL;
    GPIO_PA6_Struct.Speed = GPIO_SPEED_FREQ_HIGH;  
    GPIO_PA6_Struct.Alternate = GPIO_AF2_TIM3;
	HAL_GPIO_Init(GPIOB, &GPIO_PA6_Struct);        

    /* Initialise timer 3 with:
        x Instance TIM3
        x Prescaler of 1
        x Counter mode up
        x Timer period to generate a 10kHz signal
        x Clock division of something numerically equal to 0, i.e. div1 */
    htim3.Instance = TIM3;
    htim3.Init.Prescaler = 1;
    htim3.Init.CounterMode = TIM_COUNTERMODE_UP;
    htim3.Init.Period = 10000;
    htim3.Init.ClockDivision = TIM_CLOCKDIVISION_DIV1;             
    HAL_TIM_PWM_Init(&htim3);

    /* Configure timer 3, channel 1 with:
        x Output compare mode PWM1
        x Pulse = 0
        x OC polarity high
        x Fast mode disabled */
    timerConfigPWM.OCMode = TIM_OCMODE_PWM1;
    timerConfigPWM.Pulse = 0;
    timerConfigPWM.OCPolarity = TIM_OCPOLARITY_HIGH; 
    timerConfigPWM.OCFastMode = TIM_OCFAST_DISABLE;
    HAL_TIM_PWM_ConfigChannel(&htim3, &timerConfigPWM, TIM_CHANNEL_1);

    /* Set initial Timer 3, channel 1 compare value */
    __HAL_TIM_SET_COMPARE(&htim3, TIM_CHANNEL_1, 2500); // dodgy LED so I think this works but I'm not entirely sure
    
    /* Start Timer 3, channel 1 */
    HAL_TIM_PWM_Start(&htim3, TIM_CHANNEL_1);
}

void motor_encoder_init(void)
{
    /* Enable GPIOC clock */
    __HAL_RCC_GPIOC_CLK_ENABLE();

    /* Initialise PC0, PC1 with:
        X Pin 0|1
        X Interrupt rising and falling edge
        X No pull
        X High frequency */
    GPIO_EncoderPins_Struct.Pin = GPIO_PIN_0|GPIO_PIN_1;
    GPIO_EncoderPins_Struct.Mode = GPIO_MODE_IT_RISING_FALLING;
    GPIO_EncoderPins_Struct.Pull = GPIO_NOPULL;
    GPIO_EncoderPins_Struct.Speed = GPIO_SPEED_FREQ_HIGH;
    HAL_GPIO_Init(GPIOC, &GPIO_EncoderPins_Struct);        

    /* Set priority of external interrupt lines 0,1 to 0x0f, 0x0f
        To find the IRQn_Type definition see "MCHA3500 Windows Toolchain\workspace\STM32Cube_F4_FW\Drivers\
        CMSIS\Device\ST\STM32F4xx\Include\stm32f446xx.h" */
    HAL_NVIC_SetPriority(EXTI0_IRQn, 0x0f, 0x0f);
    HAL_NVIC_SetPriority(EXTI1_IRQn, 0x0f, 0x0f);    

    /* Enable external interrupt for lines 0, 1 */
    HAL_NVIC_EnableIRQ(EXTI0_IRQn);
    HAL_NVIC_EnableIRQ(EXTI1_IRQn);    
}


// Not sure if these work since I don't have the encoder currently
void EXTI0_IRQHandler(void)
{
    // Check if PC0 == PC1. Adjust encoder count accordingly.
    if (HAL_GPIO_ReadPin(GPIOC, GPIO_PIN_0) == HAL_GPIO_ReadPin(GPIOC, GPIO_PIN_1))
    {
        enc_count++;
    }
    else
    {
        enc_count--;
    }

    // Reset interrupt
    HAL_GPIO_EXTI_IRQHandler(GPIO_PIN_0);
}

void EXTI1_IRQHandler(void)
{
    // Check if PC0 == PC1. Adjust encoder count accordingly.
    if (HAL_GPIO_ReadPin(GPIOC, GPIO_PIN_0) == HAL_GPIO_ReadPin(GPIOC, GPIO_PIN_1))
    {
        enc_count--;
    }
    else
    {
        enc_count++;
    }

    // Reset interrupt
    HAL_GPIO_EXTI_IRQHandler(GPIO_PIN_1);
}

int32_t motor_encoder_getValue(void)
{
    return enc_count;
}
