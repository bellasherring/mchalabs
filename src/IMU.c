#include "tm_stm32_mpu6050.h"
#include "IMU.h"
#include <math.h>

#ifndef M_PI
    #define M_PI 3.14159265358979323846
#endif

// variable declarations
TM_MPU6050_t IMU_datastruct;

// function definitions
void IMU_init(void)
{
    // initialise IMU with ADO LOW, acceleration sensitivity =-4g, gyroscope +-250 deg/s
    TM_MPU6050_Init(&IMU_datastruct, TM_MPU6050_Device_0, TM_MPU6050_Accelerometer_4G, TM_MPU6050_Gyroscope_250s);
}

void IMU_read(void)
{
    // read all IMU values
    TM_MPU6050_ReadAll(&IMU_datastruct);
}

float get_accY(void)
{
    // read the values
    IMU_read();

    // convert acceleration reading to ms^-2
    float y_acc = IMU_datastruct.Accelerometer_Y *(4.0/32767.0)*9.81;

    // return the y accleration
    return y_acc;
}

float get_accZ(void)
{
    // read the values
    IMU_read();

    // convert acceleration reading to ms^-2
    float z_acc = IMU_datastruct.Accelerometer_Z *(4.0/32767.0)*9.81;

    // return the z accleration
    return z_acc;
}

float get_gyroX(void)
{
    // read the values
    IMU_read();

    // convert gyro reading to rad/s
    float x_gyro = IMU_datastruct.Gyroscope_X *(250.0/32767.0)*(M_PI/180.0);

    // return the x gyro, rad/s
    return x_gyro;
}

double get_acc_angle(void)
{
    // compute imu angle using accY and accZ using atan2
    float Z = get_accZ();
    float Y = get_accY();

    double imu_angle = -atan2(Z, Y);

    // return the imu angle
    return imu_angle;
}