#include <stdint.h>
#include <stdlib.h>
#include <math.h>
#include "stm32f4xx_hal.h"
#include "cmsis_os2.h"
#include "uart.h"
#include "pendulum.h"

// definition variables
static ADC_HandleTypeDef hadc1;
static GPIO_InitTypeDef GPIO_PB0_Struct;
static ADC_ChannelConfTypeDef channelConfigADC;

void pendulum_init(void)
{
	/* Enable ADC1 clock */
	__HAL_RCC_ADC1_CLK_ENABLE();
	/* Enable GPIOB clock */
	__HAL_RCC_GPIOB_CLK_ENABLE();
	
	/* Initialise PB0 with:
		X Pin 0
		X Analog mode
		X No pull
		X High frequency */
	GPIO_PB0_Struct.Pin = GPIO_PIN_0;
	GPIO_PB0_Struct.Mode = GPIO_MODE_ANALOG;
    GPIO_PB0_Struct.Pull = GPIO_NOPULL;
    GPIO_PB0_Struct.Speed = GPIO_SPEED_FREQ_HIGH;  
	HAL_GPIO_Init(GPIOB, &GPIO_PB0_Struct);

	 /* Initialise ADC1 with:
		x Instance ADC 1
		x Div 2 prescaler 
		x 12 bit resolution
		x Data align right
		x Continuous conversion mode disabled
		x Number of conversions = 1 */
	hadc1.Instance = ADC1;
    hadc1.Init.ClockPrescaler = ADC_CLOCK_SYNC_PCLK_DIV2;
    hadc1.Init.ContinuousConvMode = DISABLE;
    hadc1.Init.DataAlign = ADC_DATAALIGN_RIGHT;
    hadc1.Init.Resolution = ADC_RESOLUTION_12B;   
    hadc1.Init.NbrOfConversion = 1;
	HAL_ADC_Init(&hadc1);
        
	/* Configure ADC channel to:
		x Channel 8
		x Rank 1
		x Sampling time 480 cycles
		x Offset 0 */
    channelConfigADC.Channel = ADC_CHANNEL_8;
    channelConfigADC.SamplingTime = ADC_SAMPLETIME_480CYCLES;
    channelConfigADC.Rank = 1;
    channelConfigADC.Offset = 0;		
	HAL_ADC_ConfigChannel(&hadc1, &channelConfigADC);
}

float pendulum_get_voltage(void)
{
	/* Start ADC */
	HAL_ADC_Start(&hadc1);

	/* Poll for conversion. Use timeout of 0xFF. */
    HAL_ADC_PollForConversion(&hadc1, 0xFF);

	/* Get ADC value */
    uint32_t ADCResult = HAL_ADC_GetValue(&hadc1);

	/* Stop ADC */
    HAL_ADC_Stop(&hadc1);  

	/* Compute voltage from ADC reading. Hint: 2^12-1 = 4095 */
    float voltageReading = (3.3/4095.0)*ADCResult;

	/* Return the computed voltage */
	return voltageReading;
}