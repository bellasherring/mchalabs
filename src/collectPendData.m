clear
clc

if ~isempty(instrfind('Status', 'open'))
    fclose(instrfind);
end

% Create connections to each device
stm_device = serial('COM4', 'BaudRate', 115200, 'Timeout', 0.5, 'Terminator', 'LF');
fopen(stm_device);
% Call the potentiometer data-logging code. 'compose' appends the correct new-line characters to the string
txStr = compose("logPot");
fprintf(stm_device, txStr);
% Define expected number of samples
N = 400;
% Initialise data structure for data
potData.time = zeros(N,1);
potData.voltage = zeros(N,1);

% Collect the expected number of samples
for i=1:N
    % Read in data from serial connection
    rxStr = fgets(stm_device);
    % Separate string into parts
    separatedString = split(rxStr,',');
    % Convert data to numbers and store in data structure
    potData.time(i) = str2double(separatedString{1});
    potData.voltage(i) = str2double(separatedString{2});
end

% Plot results
plot(potData.time, potData.voltage, 'b+')
title('Logged Data')
ylabel('Potentiometer Voltage (V)')
xlabel('Time (s)')
ylim([-0.1 3.4])
grid on

% Teardown
fclose(stm_device);
delete(stm_device);
clear stm_device
clear stm_device