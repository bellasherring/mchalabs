function R = returnCovariance()

    % Pt1: estimating the sample mean and variance of the gyrocope readings
    load('imuData_still');
    % Unpack data
    time = imuData.time;
    accAngle = imuData.angle;
    gyroVelocity = imuData.velocity;
    voltage = imuData.voltage;
    potAngle = (voltage-2.125)*(240/3.3)*(pi/180);

    N = length(time);
    x = [gyroVelocity];

    % Compute sample mean
    mean_s_gyro = (1/N)*sum(x);
    % Compute sample variance. Use Bessel's correction
    var_s_gyro = (1/(N-1))*sum((x - mean_s_gyro)*(x - mean_s_gyro).');

    % Pt2: estimating variance of accelerometer angle measurements
    load('imuData_moving1');

    % Unpack data
    time = imuData.time;
    accAngle = imuData.angle;
    gyroVelocity = imuData.velocity;
    voltage = imuData.voltage;
    potAngle = (voltage-2.125)*(240/3.3)*(pi/180);
    N = length(time);

    % Correct angle offset
    pot_angle_init = mean(potAngle(1:200));
    acc_angle_init = mean(accAngle(1:200));
    potAngle = potAngle + (acc_angle_init - pot_angle_init);

    % Compute the angle measurement 'noise'
    angle_noise = accAngle - potAngle;

    x = [angle_noise];
    % Compute sample mean
    mean_s_angle = (1/N)*sum(x);
    % Compute sample variance. Use Bessel's correction
    var_s_angle = (1/(N-1))*sum((x - mean_s_angle)*(x - mean_s_angle).');

    % measurement noise covariance
    R = [var_s_angle zeros(size(var_s_angle)); zeros(size(var_s_gyro)) var_s_gyro];

end

