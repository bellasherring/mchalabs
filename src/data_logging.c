#include <stdint.h>
#include <stdlib.h>
#include <math.h>
#include "stm32f4xx_hal.h"
#include "cmsis_os2.h"
#include "uart.h"
#include "data_logging.h"
#include "pendulum.h"
#include "IMU.h"

// variables
uint16_t logCount;
static osTimerId_t loggingtimer_id;
static osTimerAttr_t loggingTimer_attr = 
{
    .name = "dataLoggingTimer"
};
static void (*log_function)(void);

// function declarations
static void log_pendulum(void);
static void log_imu(void);
static void log_pointer(void *argument);

// function definitions

// generic log functions -----------------------------------------------------------------------------

void logging_init(void) 
{
    // initialise timer for use with pendulum data logging
    loggingtimer_id = osTimerNew(log_pointer, osTimerPeriodic, NULL, &loggingTimer_attr);
}

static void log_pointer(void *argument) 
{
    UNUSED(argument);
    (*log_function)();
}

// specific log functions -----------------------------------------------------------------------------

static void log_pendulum(void) 
{
    // read potentiometer voltage
    float potVoltage = pendulum_get_voltage();

    // print sample time and potentiometer voltage in format [time], [voltage]
    float sampleTime = logCount*(1.0/200.0);    // in future use proper functions to get ticks and things
    printf("%f, %f \n", sampleTime, potVoltage);

    // increment log count
    logCount++;

    // stop logging once 2 seconds is reached
    if (logCount == 400)
    {
        pend_logging_stop();
    }
}

static void log_imu(void)
{
    // read imu
    IMU_read();

    // get angle
    double imu_angle = get_acc_angle();

    // get gyro X
    float gyro_X = get_gyroX();

    // read pot voltage
    float potVoltage = pendulum_get_voltage();

    // print time, angle, gyro, pot voltage
    float sampleTime = logCount*(1.0/200.0);    // in future use proper functions to get ticks and things
    printf("%f, %f, %f, %f \n", sampleTime, imu_angle, gyro_X, potVoltage);

    // increment log count
    logCount++;

    // stop logging once 5 seconds is reached
    if (logCount == 1000)
    {
        imu_logging_stop();
    }
}

// starts and stops -----------------------------------------------------------------------------

void pend_logging_start(void) 
{
    log_function = &log_pendulum;

    if(!osTimerIsRunning(loggingtimer_id))
    {
        // reset log counter
        logCount = 0;

        // start logging timer at 200 Hz
        osTimerStart(loggingtimer_id, 5);   
    }
}

void pend_logging_stop(void)
{
    // stop data logging timer
    if(osTimerIsRunning(loggingtimer_id))
    {
        osTimerStop(loggingtimer_id);
    }
}

void imu_logging_start(void)
{
    log_function = &log_imu;

    if(!osTimerIsRunning(loggingtimer_id))
    {
        // reset log counter
        logCount = 0;

        // start logging timer at 200 Hz
        osTimerStart(loggingtimer_id, 5);   
    }
}

void imu_logging_stop(void)
{
    // stop data logging timer
    if(osTimerIsRunning(loggingtimer_id))
    {
        osTimerStop(loggingtimer_id);
    }
}